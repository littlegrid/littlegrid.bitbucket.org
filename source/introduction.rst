Head over to the new www.littlegrid.net web-site
================================================

The littlegrid web-site has moved to a new faster home :-)

This page will redirect you in 10 seconds, if you aren't automatically redirected
then click to head over to `www.littlegrid.net <http://www.littlegrid.net>`_

Don't forget to update your bookmark!
